//35190-ИС Антипов Илья

package ru.unn.lesson1;
import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Сколько чисел будет?");
        int ind = in.nextInt();
        System.out.println("Введите числа");
        double [] arr1 = new double [ind];
        for (int i = 0; i < arr1.length; i++){
            arr1[i] = in.nextDouble();
            arr1[i] = arr1[i] + (arr1[i] / 100 * 10);
            }
        for (int i = arr1.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr1[j] < arr1[j + 1]) {
                    double tmp = arr1[j];
                    arr1[j] = arr1[j + 1];
                    arr1[j + 1] = tmp;
                }
            }
        }
        String arrStr = Arrays.toString(arr1);
        System.out.println(arrStr);
    }
}

